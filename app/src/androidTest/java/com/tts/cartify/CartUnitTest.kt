package com.tts.cartify

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import com.tts.cartify.data.local.CartifyDatabase
import com.tts.cartify.data.local.dao.CartDao
import com.tts.cartify.data.local.entity.ProductEntity
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CartUnitTest {


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: CartifyDatabase
    private lateinit var dao: CartDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            CartifyDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.cartDao()
    }

    @After
    fun teardown() {
        database.close()
    }


    @Test
    fun insertProductItem() = runBlocking {
        val productEntity = ProductEntity(
            "10457",
            "https://ik.imagekit.io/bfrs/tr:w-593,h-593,pr-true,cm-pad_resize,bg-FFFFFF/image_oxolloxodemo/data/S19064WBL001-1.jpg",
            1, "",
            "If you think you are unique then why should your fashion",
            "", "", "₹1,249", "10457", "Black Cut", "", ""
        )
        dao.insertProduct(productEntity)

        val cart = dao.getCart()
        println("Cart  $cart")
        assertThat(cart, hasItem(productEntity))
    }

    @Test
    fun deleteProductItem() = runBlocking {
        val productEntity = ProductEntity(
            "10457",
            "https://ik.imagekit.io/bfrs/tr:w-593,h-593,pr-true,cm-pad_resize,bg-FFFFFF/image_oxolloxodemo/data/S19064WBL001-1.jpg",
            1, "",
            "If you think you are unique then why should your fashion",
            "", "", "₹1,249", "10457", "Black Cut", "", ""
        )
        dao.insertProduct(productEntity)

        var cart = dao.getCart()
        println("Cart before delete  $cart")

        dao.deleteProductById(productEntity.id)
        cart = dao.getCart()
        println("Cart after delete  $cart")


        assertThat(cart, not(hasItem(productEntity)))
    }
}