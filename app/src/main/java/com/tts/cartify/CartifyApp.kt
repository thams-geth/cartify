package com.tts.cartify

import android.app.Application
import androidx.databinding.ktx.BuildConfig
import com.tts.cartify.di.databaseModule
import com.tts.cartify.di.networkModule
import com.tts.cartify.di.repositoryModule
import com.tts.cartify.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class CartifyApp : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startKoin {
            androidContext(this@CartifyApp)
            modules(listOf(viewModelModule, repositoryModule, databaseModule, networkModule))
        }
    }
}