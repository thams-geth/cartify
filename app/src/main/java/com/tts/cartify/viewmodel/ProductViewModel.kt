package com.tts.cartify.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tts.cartify.data.local.entity.ProductEntity
import com.tts.cartify.data.model.ProductsData
import com.tts.cartify.data.repository.Event
import com.tts.cartify.data.repository.ProductRepository
import com.tts.cartify.data.repository.Results
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductViewModel(private val productRepository: ProductRepository) : ViewModel() {

    private val _serverError = MutableLiveData<Event<String>>()
    val serverError: LiveData<Event<String>>
        get() = _serverError

    private var _loading = MutableLiveData<Event<Boolean>>()
    val loading: LiveData<Event<Boolean>> get() = _loading

    private var _productsData = MutableLiveData<Event<ProductsData>>()
    val productsData: LiveData<Event<ProductsData>> get() = _productsData

    fun getProducts() {
        viewModelScope.launch {
            _loading.value = Event(true)
            when (val result = productRepository.getProducts()) {
                is Results.Success -> {
                    _loading.value = Event(false)
                    _productsData.value = Event(result.value)
                }
                is Results.Failure -> {
                    _loading.value = Event(false)
                    _serverError.value = Event(result.statusResponse)

                }
            }
        }
    }

    private var _cart = MutableLiveData<List<ProductEntity>>()
    val cart: LiveData<List<ProductEntity>> get() = _cart

    fun getCart() {
        CoroutineScope(Dispatchers.IO).launch {
            _cart.postValue(productRepository.getCart())
        }
    }

    fun addCartProduct(product: ProductEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            productRepository.addCartProduct(product)
        }
    }

    fun updateCartProduct(product: ProductEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            productRepository.updateCartProduct(product)
        }
    }

    fun removeCartProduct(product: ProductEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            productRepository.removeCartProduct(product)
        }
    }

    fun removeCart() {
        CoroutineScope(Dispatchers.IO).launch {
            productRepository.removeCart()
        }
    }
}