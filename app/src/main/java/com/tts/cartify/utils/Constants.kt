package com.tts.cartify.utils

object Constants {
    const val SPLASH_DELAY: Long = 2000
    const val DOMAIN = "https://www.mocky.io"
    const val TIME_OUT = 60L
}