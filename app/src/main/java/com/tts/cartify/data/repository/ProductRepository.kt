package com.tts.cartify.data.repository

import com.tts.cartify.data.local.dao.CartDao
import com.tts.cartify.data.local.entity.ProductEntity
import com.tts.cartify.data.remote.ApiService

class ProductRepository(private val cartDao: CartDao, private val apiService: ApiService) : BaseRepository() {

    suspend fun getProducts() = safeApiCall { apiService.getProducts() }

    suspend fun getCart(): List<ProductEntity> {
        return cartDao.getCart()
    }

    suspend fun addCartProduct(product: ProductEntity) {
        cartDao.insertProduct(product)
    }

    suspend fun updateCartProduct(product: ProductEntity) {
        cartDao.updateProductById(product.quantity, product.id)
    }

    suspend fun removeCartProduct(product: ProductEntity) {
        cartDao.deleteProductById(product.id)
    }

    suspend fun removeCart() {
        cartDao.deleteCart()
    }
}