package com.tts.cartify.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "product")
data class ProductEntity(

    @PrimaryKey
    @field:SerializedName("id")
    val id: String,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("quantity")
    var quantity: Int,

    @field:SerializedName("thumb")
    val thumb: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("zoom_thumb")
    val zoomThumb: String? = null,

    @field:SerializedName("special")
    val special: String? = null,

    @field:SerializedName("price")
    val price: String? = null,

    @field:SerializedName("product_id")
    val productId: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("href")
    val href: String? = null,

    @field:SerializedName("sku")
    val sku: String? = null
)