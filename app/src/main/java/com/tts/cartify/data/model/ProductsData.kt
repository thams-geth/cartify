package com.tts.cartify.data.model

import com.google.gson.annotations.SerializedName
import com.tts.cartify.data.local.entity.ProductEntity

data class ProductsData(

    @field:SerializedName("products")
    val products: List<ProductEntity?>? = null
)


