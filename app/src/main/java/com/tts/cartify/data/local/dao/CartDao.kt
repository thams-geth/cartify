package com.tts.cartify.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tts.cartify.data.local.entity.ProductEntity

@Dao
interface CartDao {

    @Query("SELECT * FROM product")
    suspend fun getCart(): List<ProductEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProduct(product: ProductEntity): Long

    @Query("UPDATE product SET quantity = :quantity WHERE id = :id")
    suspend fun updateProductById(quantity: Int, id: String): Int

    @Query("DELETE FROM product WHERE id=:noteId")
    suspend fun deleteProductById(noteId: String): Int

    @Query("DELETE FROM product")
    suspend fun deleteCart()
}