package com.tts.cartify.data.remote

import com.tts.cartify.data.model.ProductsData
import retrofit2.http.GET

interface ApiService {

    @GET(getProductURL)
    suspend fun getProducts(): ProductsData

    companion object {
        const val getProductURL = "/v2/5def7b172f000063008e0aa2"
    }
}