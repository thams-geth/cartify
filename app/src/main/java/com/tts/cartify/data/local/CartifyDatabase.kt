package com.tts.cartify.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tts.cartify.data.local.dao.CartDao
import com.tts.cartify.data.local.entity.ProductEntity

@Database(entities = [ProductEntity::class], version = 1, exportSchema = false)
abstract class CartifyDatabase : RoomDatabase() {
    abstract fun cartDao(): CartDao
}