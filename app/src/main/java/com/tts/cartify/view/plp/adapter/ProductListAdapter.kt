package com.tts.cartify.view.plp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tts.cartify.data.local.entity.ProductEntity
import com.tts.cartify.databinding.ItemProductBinding
import com.tts.cartify.listeners.ProductClickListener
import com.tts.cartify.utils.visible

class ProductListAdapter(private val productClickListener: ProductClickListener) :
    ListAdapter<ProductEntity, ProductViewHolder>(ProductDiffUtils) {

    private var cartList: List<ProductEntity> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            ItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.setData(getItem(position), productClickListener, cartList)
    }

    object ProductDiffUtils : DiffUtil.ItemCallback<ProductEntity>() {
        override fun areItemsTheSame(oldItem: ProductEntity, newItem: ProductEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ProductEntity, newItem: ProductEntity): Boolean {
            return oldItem == newItem
        }

    }

    fun setUpdatedCartList(list: List<ProductEntity>) {
        cartList = list
        for (i in 0 until itemCount) {
            val product = getItem(i)
            val isAvailable = list.firstOrNull { it.id == product?.id }
            isAvailable?.let {
                notifyItemChanged(i)
            }
        }
    }
}

class ProductViewHolder(private val view: ItemProductBinding) : RecyclerView.ViewHolder(view.root) {
    fun setData(
        productEntity: ProductEntity,
        productClickListener: ProductClickListener,
        cartList: List<ProductEntity>
    ) {
        view.productData = productEntity
        Glide.with(view.ivProduct.context).load(productEntity.image).into(view.ivProduct)
//        var quantity: Int = Integer.parseInt(view.txtQuantity.text.toString())

        val cartItem = cartList.firstOrNull { it.id == productEntity.id }
        if (cartItem != null) {
            view.txtAdd.visible(false)
            view.llAddandMinus.visible(true)
            productEntity.quantity = cartItem.quantity
            view.txtQuantity.text = cartItem.quantity.toString()
        } else {
            view.txtAdd.visible(true)
            view.llAddandMinus.visible(false)
        }
        view.txtAdd.setOnClickListener {
            view.txtAdd.visible(false)
            view.llAddandMinus.visible(true)
            productClickListener.onCartAdd(bindingAdapterPosition, productEntity)
        }
        view.ivIncrement.setOnClickListener {
            if (productEntity.quantity >= 5) {
                return@setOnClickListener
            } else {
                productEntity.quantity++
                productClickListener.onCartIncrease(bindingAdapterPosition, productEntity)
            }
        }
        view.ivDecrement.setOnClickListener {
            if (productEntity.quantity <= 1) {
                view.txtAdd.visible(true)
                view.llAddandMinus.visible(false)
                productClickListener.onCartRemove(bindingAdapterPosition, productEntity)
                return@setOnClickListener
            } else {
                productEntity.quantity--
                productClickListener.onCartDecrease(bindingAdapterPosition, productEntity)
            }
        }
    }
}

