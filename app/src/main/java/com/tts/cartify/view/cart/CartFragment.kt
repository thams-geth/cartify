package com.tts.cartify.view.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tts.cartify.R
import com.tts.cartify.databinding.FragmentCartBinding
import com.tts.cartify.utils.toast
import com.tts.cartify.view.cart.adapter.CartAdapter
import com.tts.cartify.viewmodel.ProductViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class CartFragment : Fragment() {

    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!
    private val productViewModel: ProductViewModel by viewModel()

    private var cartAdapter: CartAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCartAdapter()
        observeLiveData()
        getLocalCart()
    }

    private fun setCartAdapter() {
        cartAdapter = CartAdapter()
        binding.rvCart.adapter = cartAdapter
    }

    private fun getLocalCart() {
        productViewModel.getCart()
    }

    private fun observeLiveData() {
        with(productViewModel) {
            cart.observe(viewLifecycleOwner, {
                if (it.isEmpty()) {
                    toast(requireContext(), getString(R.string.cart_is_empty))
                }
                cartAdapter?.submitList(it)
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}