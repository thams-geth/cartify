package com.tts.cartify.view.plp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.cartify.R
import com.tts.cartify.data.local.entity.ProductEntity
import com.tts.cartify.data.repository.EventObserver
import com.tts.cartify.databinding.FragmentProductListBinding
import com.tts.cartify.listeners.ProductClickListener
import com.tts.cartify.utils.toast
import com.tts.cartify.utils.visible
import com.tts.cartify.view.plp.adapter.ProductListAdapter
import com.tts.cartify.viewmodel.ProductViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class ProductListFragment : Fragment() {

    private var _binding: FragmentProductListBinding? = null
    private val binding get() = _binding!!
    private val productViewModel: ProductViewModel by viewModel()

    private var productListAdapter: ProductListAdapter? = null

    private val productClickListener = object : ProductClickListener {

        override fun onCartAdd(position: Int, product: ProductEntity) {
            productViewModel.addCartProduct(product)
            getLocalCart()
        }

        override fun onCartIncrease(position: Int, product: ProductEntity) {
            productViewModel.updateCartProduct(product)
            getLocalCart()
        }

        override fun onCartDecrease(position: Int, product: ProductEntity) {
            productViewModel.updateCartProduct(product)
            getLocalCart()
        }

        override fun onCartRemove(position: Int, product: ProductEntity) {
            productViewModel.removeCartProduct(product)
            getLocalCart()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProductAdapter()
        observeLiveData()
        getProducts()
        getLocalCart()
    }

    private fun setProductAdapter() {
        productListAdapter = ProductListAdapter(productClickListener)
        binding.rvProduct.adapter = productListAdapter
    }

    private fun getProducts() {
        productViewModel.getProducts()
    }

    private fun getLocalCart() {
        productViewModel.getCart()
    }

    private fun observeLiveData() {
        with(productViewModel) {
            loading.observe(viewLifecycleOwner, EventObserver {
                binding.progressbar.visible(it)
            })
            productsData.observe(viewLifecycleOwner, EventObserver {
                if (it.products != null) {
                    productListAdapter?.submitList(it.products)
                }
            })
            serverError.observe(viewLifecycleOwner, EventObserver {
                toast(requireContext(), it)
            })
            cart.observe(viewLifecycleOwner, {
                productListAdapter?.setUpdatedCartList(it)
                // Update cart count in toolbar
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        return inflater.inflate(R.menu.menu_plp, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_cart -> {
                findNavController().navigate(R.id.cartFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}