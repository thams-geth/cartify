package com.tts.cartify.view.cart.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tts.cartify.data.local.entity.ProductEntity
import com.tts.cartify.databinding.ItemCartBinding

class CartAdapter : ListAdapter<ProductEntity, CartViewHolder>(ProductDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        return CartViewHolder(
            ItemCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.setData(getItem(position))
    }

    object ProductDiffUtils : DiffUtil.ItemCallback<ProductEntity>() {
        override fun areItemsTheSame(oldItem: ProductEntity, newItem: ProductEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ProductEntity, newItem: ProductEntity): Boolean {
            return oldItem == newItem
        }

    }
}

class CartViewHolder(private val view: ItemCartBinding) : RecyclerView.ViewHolder(view.root) {
    fun setData(productEntity: ProductEntity) {
        view.productData = productEntity
        Glide.with(view.ivProduct.context).load(productEntity.image).into(view.ivProduct)
    }
}

