package com.tts.cartify.listeners

import com.tts.cartify.data.local.entity.ProductEntity


interface ProductClickListener {
    fun onCartAdd(position: Int, product: ProductEntity)
    fun onCartIncrease(position: Int, product: ProductEntity)
    fun onCartDecrease(position: Int, product: ProductEntity)
    fun onCartRemove(position: Int, product: ProductEntity)
}
