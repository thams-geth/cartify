package com.tts.cartify.di

import com.tts.cartify.viewmodel.ProductViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { ProductViewModel(get()) }
}

val repositoryModule = module {
    single { provideProductRepository(get(), get()) }
}

val databaseModule = module {
    single { provideDatabase(androidApplication()) }
    single { provideCartDao(get()) }
}

val networkModule = module {
    single { provideLoggingInterceptor() }
    single { provideOkHttpClient(get()) }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }
}