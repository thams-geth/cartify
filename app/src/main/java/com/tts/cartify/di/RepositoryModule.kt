package com.tts.cartify.di

import com.tts.cartify.data.local.dao.CartDao
import com.tts.cartify.data.remote.ApiService
import com.tts.cartify.data.repository.ProductRepository

fun provideProductRepository(cartDao: CartDao, apiService: ApiService): ProductRepository {
    return ProductRepository(cartDao, apiService)
}

