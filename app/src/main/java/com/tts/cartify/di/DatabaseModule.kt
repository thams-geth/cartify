package com.tts.cartify.di

import android.content.Context
import androidx.room.Room
import com.tts.cartify.data.local.CartifyDatabase
import com.tts.cartify.data.local.dao.CartDao

fun provideDatabase(context: Context): CartifyDatabase {
    return Room.databaseBuilder(context, CartifyDatabase::class.java, "cartify.db").build()
}

fun provideCartDao(db: CartifyDatabase): CartDao {
    return db.cartDao()
}